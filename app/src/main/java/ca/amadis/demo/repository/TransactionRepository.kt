package ca.amadis.demo.repository
import androidx.lifecycle.LiveData
import ca.amadis.demo.data.TransactionDao
import ca.amadis.demo.data.TransactionEntity


class TransactionRepository (private val transactionDao: TransactionDao){
    val readAllTransactions : LiveData<List<TransactionEntity>> =transactionDao.readAllData()


    suspend fun addTransaction(transaction: TransactionEntity){

        transactionDao.addTransaction(transaction)
    }


}