
package ca.amadis.demo.amadissdk.secureclient

interface SecureClientListener {

    fun onAuthenticateDone()
    fun onAuthenticateFailed(error: Int, message: String)

    fun onProvisionDone()
    fun onProvisionFailed(error: Int, message: String)

    fun onInitializeDone()
    fun onInitializeFailed(error: Int, message: String)

    fun onStartDone()

    fun onTerminateDone()

    fun onPINDone()
}