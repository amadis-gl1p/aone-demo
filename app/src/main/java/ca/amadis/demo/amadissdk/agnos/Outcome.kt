package ca.amadis.tester.agnos

import android.util.Log
import ca.amadis.agnos.sdk.ola.types.OlaUiRequest
import ca.amadis.agnos.sdk.Agnos
import ca.amadis.agnos.sdk.ola.types.OlaError
import ca.amadis.agnos.sdk.ola.types.OlaOutcomeParameter
import ca.amadis.agnos.sdk.ola.types.OlaTag
import ca.amadis.agnos.sdk.ola.utils.Holders

object Outcome {
    class Tag(val tag: Int, val ascii: Boolean = false) {
        var name = "0x%X".format(tag)
        var content: String = ""
        var err: OlaError = OlaError.OLA_MISSING_DATA
    }
    private val outcomeTags = arrayOf(
        Tag(OlaTag.APPLICATION_LABEL, true),
        Tag(OlaTag.DF_NAME),
        Tag(OlaTag.TVR),
        Tag(OlaTag.TRACK2_EQUIVALENT_DATA),
        Tag(OlaTag.TRANSACTION_STATUS_INFORMATION),
        Tag(OlaTag.EMV_PAN),
        Tag(OlaTag.PAN_SEQUENCE_NUMBER),
        Tag(OlaTag.ISSUER_APPLICATION_DATA),
        Tag(OlaTag.APPLICATION_CRYPTOGRAM),
        Tag(OlaTag.CRYPTOGRAM_INFORMATION_DATA),
        Tag(OlaTag.APPLICATION_TRANSACTION_COUNTER),
        //Tag(OlaTag.TTQ),
        //Tag(OlaTag.FORM_FACTOR_INDICATOR),
        //Tag(OlaTag.CUSTOMER_EXCLUSIVE_DATA)
    )

    fun showOutcome(agnos: Agnos, previousResult: OlaError) {

        Log.v("OlaOutcome", "showOutcome - previous result before showOutcome(): $previousResult")
        val outcomeHolder = Holders.SingleObjectHolder<OlaOutcomeParameter>()
        val ret = agnos.olaContactlessGetOutcome(outcomeHolder)
        Log.v("OlaOutcome", "showOutcome - getOutcome() ret: $ret")

        val outcome = outcomeHolder.get()
        Log.v("OlaOutcome", "showOutcome - outcome: " + outcome?.outcome)
        Log.v("OlaOutcome", "showOutcome - startingPoint: " + outcome?.startingPoint)
        Log.v("OlaOutcome", "showOutcome - onlineResponseData: " + outcome?.onlineResponseData)
        Log.v("OlaOutcome", "showOutcome - cvm: " + outcome?.cvm)

        Log.v("OlaOutcome", "showOutcome - UIReqOnOutcomePresent: " + outcome?.UIReqOnOutcomePresent)
        if (outcome?.UIReqOnOutcomePresent == true) {
            showUIRequestOutcome(agnos)
        }

        Log.v("OlaOutcome", "showOutcome - UIReqOnRestartPresent: " + outcome?.UIReqOnRestartPresent)
        if (outcome?.UIReqOnRestartPresent == true) {
            showUIRequestRestart(agnos)
        }

        Log.v("OlaOutcome", "showOutcome - dataRecordPresent: " + outcome?.dataRecordPresent)
        Log.v("OlaOutcome", "showOutcome - discretionaryDataPresent: " + outcome?.discretionaryDataPresent)
        Log.v("OlaOutcome", "showOutcome - alternateInterfacePreference: " + outcome?.alternateInterfacePreference)
        Log.v("OlaOutcome", "showOutcome - fieldOffReq: " + outcome?.fieldOffReq)
        Log.v("OlaOutcome", "showOutcome - removalTimeout: " + outcome?.removalTimeout)
    }

    private fun showUIRequestOutcome(agnos: Agnos) {

        Log.v("OlaOutcome", "showUIRequestOutcome")
        val holder = Holders.SingleObjectHolder<OlaUiRequest>()
        val ret = agnos.olaContactlessGetUIRequestUponOutcome(holder)
        Log.v("OlaOutcome", "showUIRequestOutcome - getUIRequestUponOutcome() ret: $ret")
        if (ret == OlaError.OLA_OK) {
            val req = holder.get()
            if (req != null) {
                Log.v("OlaOutcome", "showUIRequestOutcome - messageIdentifier: " + req.messageIdentifier)
                Log.v("OlaOutcome", "showUIRequestOutcome - status: " + req.status)
                Log.v("OlaOutcome", "showUIRequestOutcome - holdTime: " + req.holdTime)
            }
        }
    }

    private fun showUIRequestRestart(agnos: Agnos) {

        Log.v("OlaOutcome", "showUIRequestRestart")
        val holder = Holders.SingleObjectHolder<OlaUiRequest>()
        val ret = agnos.olaContactlessGetUIRequestRestart(holder)
        if (ret == OlaError.OLA_OK) {
            val req = holder.get()
            if (req != null) {
                Log.v("OlaOutcome", "showUIRequestRestart - getUIRequestRestart() ret: $ret")
                Log.v("OlaOutcome", "showUIRequestRestart - messageIdentifier: " + req.messageIdentifier    )
                Log.v("OlaOutcome", "showUIRequestRestart - status: " + req.status)
                Log.v("OlaOutcome", "showUIRequestRestart - holdTime: " + req.holdTime)
            }
        }
    }
}