

import java.lang.StringBuilder

object Print {

    fun printHex(bytes: ByteArray?): String {

        if (bytes == null) {
            return "null";
        }

        val sb = StringBuilder()
        for (b in bytes) {
            sb.append(String.format("%02x", b))
        }
        return sb.toString()
    }
}