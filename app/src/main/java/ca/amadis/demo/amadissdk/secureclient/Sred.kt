package ca.amadis.demo.amadissdk.secureclient
import android.util.Base64
import android.util.Log
import ca.amadis.agnos.sdk.Agnos
import java.security.KeyFactory
import java.security.spec.MGF1ParameterSpec
import java.security.spec.PKCS8EncodedKeySpec
import javax.crypto.Cipher
import javax.crypto.spec.OAEPParameterSpec
import javax.crypto.spec.PSource
import javax.crypto.spec.SecretKeySpec

class Sred {

    companion object {

        const val TAG = "Sred"

        fun exportKCV(agnos: Agnos) {

            Log.v("Sred", "exportKCV")

            val data: ByteArray? = agnos.exportKCV()
            val kcv = data?.copyOfRange(2, data.size)
            Log.v(TAG, "exportKCV - KCV: ${Print.printHex(kcv)}")
        }

        fun exportHashedPAN(agnos: Agnos) {

            Log.v("Sred", "exportHashedPAN")

            val data: ByteArray? = agnos.exportHashedPAN()
            val hpan = data?.copyOfRange(2, data.size)
            Log.v(TAG, "exportHashedPAN - Hashed PAN: ${Print.printHex(hpan)}")
        }

        private fun privateKey() =
            "*******************************************"

        fun decryptDataBlock(agnos: Agnos) {

            Log.v("Sred", "decryptDataBlock")

            /* Create private key from PEM */
            var prvKeyPEM = privateKey()
            prvKeyPEM = prvKeyPEM.replace("-----BEGIN RSA PRIVATE KEY-----", "")
            prvKeyPEM = prvKeyPEM.replace("-----END RSA PRIVATE KEY-----", "")
            prvKeyPEM = prvKeyPEM.replace("\n", "")

            val prvKeyDER = Base64.decode(prvKeyPEM, Base64.DEFAULT)
            val prvKeySpecs = PKCS8EncodedKeySpec(prvKeyDER)
            val kf = KeyFactory.getInstance("RSA")
            val prvKey = kf.generatePrivate(prvKeySpecs)

            /* Decode blob */
            val data = agnos.exportCardData()

            // Extract Key ID
            var index = 0
            val keyIdSz = data[index].toUByte().toInt() * 256 + data[index + 1].toUByte().toInt()
            index += 2

            val keyId = data.copyOfRange(index, index + keyIdSz)
            index += keyIdSz

            // Extract wrapped key
            index += 2
            val keyCrypt = data.copyOfRange(index, index + 512)
            index += 512

            // Extract encrypted data
            val dataSz = data[index].toUByte().toInt() * 256 + data[index + 1].toUByte().toInt()
            index += 2
            val dataCrypt = data.copyOfRange(index, index + dataSz)
            index += dataSz

            // Extract HMAC key ID
            val hmacKeyIdSz =
                data[index].toUByte().toInt() * 256 + data[index + 1].toUByte().toInt()
            index += 2
            val hmacKeyId = data.copyOfRange(index, index + hmacKeyIdSz)

            index += hmacKeyIdSz

            // Extract HMAC
            index += 2
            val hmac = data.copyOfRange(index, index + 32)
            index += 32

            /* Decrypt key */
            val cipher = Cipher.getInstance("RSA/ECB/OAEPPadding")
                .apply {
                    // To use SHA-256 the main digest and SHA-1 as the MGF1 digest
                    init(
                        Cipher.DECRYPT_MODE,
                        prvKey,
                        OAEPParameterSpec(
                            "SHA-256",
                            "MGF1",
                            MGF1ParameterSpec.SHA1,
                            PSource.PSpecified.DEFAULT
                        )
                    )
                    // To use SHA-256 for both digests
                    init(
                        Cipher.DECRYPT_MODE,
                        prvKey,
                        OAEPParameterSpec(
                            "SHA-256",
                            "MGF1",
                            MGF1ParameterSpec.SHA256,
                            PSource.PSpecified.DEFAULT
                        )
                    )
                }

            val keyPlain = cipher.doFinal(keyCrypt)

            val keyAESSpec = SecretKeySpec(keyPlain, "AES")
            val cipherAES = Cipher.getInstance("AES/ECB/NoPadding")
                .apply {

                    init(Cipher.DECRYPT_MODE, keyAESSpec)
                }

            val clear = cipherAES.doFinal(dataCrypt)
            Log.i(TAG, "clear: ${java.lang.String(String(clear)).trim()}")

        }
    }
}