package ca.amadis.demo.amadissdk.secureclient
import android.content.Context
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import ca.amadis.secclt.BuildConfig.BUILD_TYPE

import ca.amadis.secclt.sdk.SecureClient
import ca.amadis.secclt.sdk.IAppSecurityCbk

class SecureClient(private var context: Context,
                   private val activity: AppCompatActivity,
                   private val listener: SecureClientListener
) {

    class AOneAppSecurityCbk(private val activity: AppCompatActivity) : IAppSecurityCbk {
        override fun getApplicationInfo(): String {
            Log.v(this::class.java.simpleName, "getApplicationInfo")
            return "{}"
        }

        override fun notifyError(error: Int) {
            Log.v(this::class.java.simpleName, "notifyError - error: $error")
        }

        override fun notifyException(e: Exception) {
            Log.v(this::class.java.simpleName, "notifyException - exception: $e")
        }

        override fun notifyExit() {
            Log.v(this::class.java.simpleName, "notifyExit")
        }

        override fun notifyRemediation(data: String?) {
            Log.v(this::class.java.simpleName, "notifyRemediation")
        }

        override fun pinEnterResult(params: Any?,
                                    status: IAppSecurityCbk.PinStatus,
                                    pin: ByteArray?,
                                    error: Int) {
            Log.v(this::class.java.simpleName,
                  "pinEnterResult - params: $params, status: $status, error: $error")

            //(activity as MainActivity).waitAndClear()
        }

        override fun syncExecuteResult(params: Any?, error: Int) {
            Log.v(this::class.java.simpleName,
                  "syncExecuteResult - error: $error, params: $params")
        }
    }

    private var secureClient: SecureClient

    init {
        try {
            Log.v(this::class.java.simpleName, "init   ")
            secureClient = SecureClient.getInstance(AOneAppSecurityCbk(activity))
        } catch (e: Exception) {
            Log.v(this::class.java.simpleName, "init - exception: $e")
            throw e
        }
    }

    private fun getSecureClientDir() : String {

        return context.filesDir.toString() + "/assets/amadis/secclt"
    }

    /**
     *
     *
     */
    fun authenticate() {

        Log.v(this::class.java.simpleName, "authenticate")

        try {
            val rc = secureClient.authenticate(activity)
            Log.v(this::class.java.simpleName, "authenticate - return code: $rc")
            val error = SecureClientError.getErrorByValue(rc)
            Log.v(
                this::class.java.simpleName,
                "authenticate - error: ${error.value} - ${error.message}"
            )

            if (error.value != 0) {
                listener.onAuthenticateFailed(error.value, error.message)
                return
            }

        } catch (e: Exception) {
            Log.v(this::class.java.simpleName, "initialize - exception: $e")
        }

        listener.onAuthenticateDone();
    }

    fun isAuthenticated(): Int {

        return secureClient.isAuthenticated()
    }

    /**
     * This function handles secure client provisioning. The provisioning step should be performed
     * once in the application life time or if some parameters have changed during the application
     * lifetime.
     *
     * The only mean to determine if provisioning has already been performed is to verify if a
     * "*.bin" file has already been created in the system.
     */
    fun provision(force: Boolean) {

        Log.v(this::class.java.simpleName, "provision")

        val ext = if (BUILD_TYPE == "debug" || BUILD_TYPE == "release") "dev" else "prod"

        /* Provisioning parameters */
        val settings = ************************

        /* Delete existing content */
        if (force) {
            secureClient.clearProvision()
        }

        if (!secureClient.isProvisioned()) {
            try {
                val rc = secureClient.provision(settings)
                Log.v(this::class.java.simpleName, "provision - return code: $rc")
                val error = SecureClientError.getErrorByValue(rc)
                Log.v(this::class.java.simpleName,
                      "provision - error: ${error.value} - ${error.message}")

                if (error.value != 0) {
                    listener.onProvisionFailed(error.value, error.message)
                    return
                }

            } catch (e: Exception) {
                Log.v(this::class.java.simpleName, "provision - exception: $e")
            }
        }

        listener.onProvisionDone()
    }

    /**
     *
     */
    fun isProvisioned(): Boolean {

        return secureClient.isProvisioned()
    }

    /**
     * Initialize the secure client. This should be performed at each application startup
     */
    fun initialize() {

        Log.v(this::class.java.simpleName, "initialize")

        try {
            val rc = secureClient.clientInitialize()
            Log.v(this::class.java.simpleName, "initialize - return code: $rc")
            val error = SecureClientError.getErrorByValue(rc)
            Log.v(
                this::class.java.simpleName,
                "initialize - error: ${error.value} - ${error.message}"
            )

            if (error.value != 0) {
                listener.onInitializeFailed(error.value, error.message)
                return
            }

        } catch (e: Exception) {
            Log.v(this::class.java.simpleName, "initialize - exception: $e")
        }

        listener.onInitializeDone();
    }

    /**
     *
     */
    fun isInitialized(): Boolean {

        return secureClient.isInitialized()
    }

    /**
     * Starts the attestation and monitoring service
     */
    fun startAttestationAndMonitoring() {

        val rc = secureClient.clientStart()
        Log.v(this::class.java.simpleName, "startAttestationAndMonitoring - return code: $rc")
        val error = SecureClientError.getErrorByValue(rc)
        Log.v(this::class.java.simpleName,
              "startAttestationAndMonitoring - error: ${error.value} - ${error.message}"
        )

        listener.onStartDone()
    }

    /**
     * Terminates the attestation and monitoring service
     */
    fun terminateAttestationAndMonitoring() {

        val rc = secureClient.clientTerminate()
        Log.v(this::class.java.simpleName, "terminateAttestationAndMonitoring - return code: $rc")
        val error = SecureClientError.getErrorByValue(rc)
        Log.v(this::class.java.simpleName,
              "terminateAttestationAndMonitoring - error: ${error.value} - ${error.message}"
        )

        listener.onTerminateDone()
    }

    fun enterPin(amount: Long) {

        var rc = secureClient.pinEnter(activity,
            "$ " + (amount / 100).toString() + "." + String.format("%02d", amount % 100),
            "Please enter PIN",
            4, 4,
            30)

        Log.v(this::class.java.simpleName, "provisionAndInit - pinEnter return code: $rc")
    }
}
