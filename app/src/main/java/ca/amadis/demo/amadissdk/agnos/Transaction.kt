package ca.amadis.tester.agnos

import android.util.Log
import ca.amadis.agnos.sdk.Agnos
import ca.amadis.agnos.sdk.ola.types.*
import ca.amadis.agnos.sdk.ola.utils.Holders
import ca.amadis.demo.amadissdk.secureclient.Sred
import ca.amadis.demo.amadissdk.utils.Utils
import ca.amadis.demo.ui.MainActivity


import kotlin.experimental.or

class Transaction(private val activity: MainActivity, val agnos: Agnos)  {
    private var amount: Long = 0
      class Tag(val tag: Int, val nameIndicator : String="", val ascii: Boolean = false) {
        var name = "0x%X".format(tag)
        var content: String = ""
        var err: OlaError = OlaError.OLA_MISSING_DATA
    }
    private val outcomeTags = arrayOf(
        Tag(OlaTag.APPLICATION_LABEL, "Application Label",ascii = true),
        Tag(OlaTag.DF_NAME,"Application"),
        Tag(OlaTag.TVR,"Terminal Verification Results"),
        Tag(OlaTag.TRACK2_EQUIVALENT_DATA,"Track2"),
        Tag(OlaTag.TRANSACTION_STATUS_INFORMATION,"Transaction Status Information"),
        Tag(OlaTag.EMV_PAN,"PAN"),
        Tag(OlaTag.PAN_SEQUENCE_NUMBER,"PAN Sequence Number"),
        Tag(OlaTag.ISSUER_APPLICATION_DATA,"Issuer Application Data"),
        Tag(OlaTag.APPLICATION_CRYPTOGRAM,"Application Cryptogram"),
        Tag(OlaTag.CRYPTOGRAM_INFORMATION_DATA,"Cryptogram Information Data"),
        Tag(OlaTag.APPLICATION_TRANSACTION_COUNTER,"Application Transaction Counter"),
    )

    private val clcEMV_CARD = 0x04


    var olaTagOutPut : String = ""


    /**
     * Configures OLA layer (key files paths, trace, etc...)
     */


    /**
     * Configures terminal permanent data
     */
    fun terminalConfig() {

        Log.v("Transaction", "terminalConfig")
        terminalOlaConfig()
        terminalContactlessConfig()
        terminalPublicKeyConfig()

        val versions = agnos.olaAgnosVersions
        Log.v("Transaction", "terminalConfig - versions: $versions")
    }

    private fun terminalOlaConfig() {

        Log.v("Transaction", "terminalOlaConfig")
        agnos.olaInitializeAtStartUp(Config.getOlaConfig()!!)
    }

    private fun terminalContactlessConfig() {

        Log.v("Transaction", "terminalContactlessConfig")
        agnos.olaContactlessFlushAIDSupported()
        for (config in Config.contactlessCfg) {
            agnos.olaContactlessAddAIDSupported(
                config.aid,
                config.partial,
                config.kernelId.toByte(),
                config.additionalData
            )
        }
        agnos.olaContactlessCommitSupportedAIDs()
    }

    private fun terminalPublicKeyConfig() {

        Log.v("Transaction", "terminalPublicKeyConfig")
        agnos.olaPublicKeyFlush()

        for (config in Config.publicKeyCfg) {
            val maxReached = Holders.SingleObjectHolder(false)
            val key: PublicKeyData = PublicKeyData(
                config.rid,
                config.idx,
                config.modulus,
                config.exponentValue,
                config.expirDate
            )
            agnos.olaPublicKeyAdd(key, null, maxReached)
        }

        agnos.olaPublicKeyCommit()
    }

    fun setAmount(amount: Long) {

        this.amount = amount
    }

    fun run(): Holders.SingleObjectHolder<OlaOutcomeParameter>  {


        terminalConfig()

        Log.v("Transaction", "run")


        txnSetTransactionRelatedData()
        txnPreProcess()
        txnGetCard()

        doTransaction()

        val outcome = Holders.SingleObjectHolder<OlaOutcomeParameter>()
        agnos.olaContactlessGetOutcome(outcome)
        outcome.get()?.outcome?.name?.let { println(it) }
        if (outcome.get()?.outcome == OlaOutcomeParameter.Outcome.Approved ||
            outcome.get()?.outcome == OlaOutcomeParameter.Outcome.OnlineRequest) {
            //activity.switchLed(4, 1, 4)
        } else {

        }

        Log.v("Transaction", "doTransaction - cvm: " + outcome.get()?.cvm)
        if (outcome.get()?.cvm == OlaOutcomeParameter.CVMethodContactless.OnlinePin) {
          //  activity.enterPin(amount)
        } else {
          //  activity.waitAndClear()
        }
        //return OlaError.OLA_OK

        return outcome

    }

    private fun  decimalToBcd(num: Long): ByteArray {

        var lnum = num

        require(lnum >= 0) {
            "The method decimalToBcd doesn't support negative numbers." +
                    " Invalid argument: " + lnum
        }

        var digits = 0
        var temp = lnum
        while (temp != 0L) {
            digits++
            temp /= 10
        }

        val byteLen = 6 //if (digits % 2 == 0) digits / 2 else (digits + 1) / 2
        val bcd = ByteArray(byteLen)
        for (i in 0 until digits) {
            val tmp = (lnum % 10).toByte()
            if (i % 2 == 0) {
                bcd[i / 2] = tmp
            } else {
                bcd[i / 2] = bcd[i / 2] or (tmp.toInt() shl 4).toByte()
            }
            lnum /= 10
        }

        for (i in 0 until byteLen / 2) {
            val tmp = bcd[i]
            bcd[i] = bcd[byteLen - i - 1]
            bcd[byteLen - i - 1] = tmp
        }

        return bcd
    }

    private fun txnSetTransactionRelatedData() {

        Log.v("Transaction", "txnSetTransactionRelatedData")
        agnos.olaEmvSetTag(OlaTag.TRANSACTION_DATE, byteArrayOf(0x20, 0x10, 0x28))                          // 9A
        agnos.olaEmvSetTag(OlaTag.TRANSACTION_TIME, byteArrayOf(0x12, 0x00, 0x00))                          // 9F21
        agnos.olaEmvSetTag(OlaTag.AMOUNT_AUTHORISED, decimalToBcd(this.amount))                             // 9F02
        agnos.olaEmvSetTag(OlaTag.AMOUNT_OTHER_NUM, byteArrayOf(0x00, 0x00, 0x00, 0x00, 0x00, 0x50))        // 9F03
        agnos.olaEmvSetTag(OlaTag.TRANSACTION_CURRENCY_CODE, byteArrayOf(0x08, 0x40))                       // 5F2A
        agnos.olaEmvSetTag(OlaTag.TRANSACTION_CURRENCY_EXPONENT, byteArrayOf(0x02))                         // 5F36
        agnos.olaEmvSetTag(OlaTag.TRANSACTION_TYPE, byteArrayOf(0x00))                                      // 9C

        /*
         * Other possible tags to set here:
         * Account Type (5F57)
         * Transaction Category Code (9F53)
         *
         * All other tags must be set through the combinations!
         */

        agnos.olaEmvSetTag(0x9F53, byteArrayOf(0x11));
        agnos.olaEmvSetTag(0x9F7C, byteArrayOf(0x55, 0x44, 0x33, 0x22, 0x11));
    }

    private fun txnPreProcess(): OlaError {

        Log.v("Transaction", "txnPreProcess")
        val ret = agnos.olaContactlessPreprocess()
        if (ret != OlaError.OLA_OK) {
            Log.e("Transaction", "txnPreProcess - contactless pre-process failed")
        }
        return ret
    }

    private fun txnGetCard(): Int {

        Log.v("Transaction", "txnGetCard - **** PRESENT CARD ****")
        val foundTechnos = agnos.technoPolling(20)
        Log.v("Transaction", "txnGetCard - result of foundTechnos: $foundTechnos")
        return if (foundTechnos == 4) {
            clcEMV_CARD //TODO remove hard-coding
        } else {
            0
        }
    }

    private fun txnGetUsefulInfo() {

        /* Useful info */
        val errorHolder = Holders.SingleObjectHolder<OlaErrorIndicator>()
        agnos.olaContactlessGetErrorIndicator(errorHolder)

        val errorIndicator: OlaErrorIndicator? = errorHolder.get()
        val errorL2 = errorIndicator?.errorL2
        println("errorL2="+ errorL2?.name)

        val uiRequestHolder = Holders.SingleObjectHolder<OlaUiRequest>()
        agnos.olaContactlessGetUIRequestUponOutcome(uiRequestHolder)



    }

    private fun doTransaction(): OlaError {

        Log.v("Transaction", "doTransaction - buildCandidateList")
        val nbCandidatesHolder = Holders.SingleObjectHolder<Int>()
        var ret = agnos.olaContactlessBuildCandidateList(nbCandidatesHolder)
        if (ret != OlaError.OLA_OK) {
            Outcome.showOutcome(agnos, ret)
        }

        Log.v("Transaction", "doTransaction - nb of candidates: ${nbCandidatesHolder.get()}")
        Log.v("Transaction", "doTransaction - finalSelectCandidate")
        val kernel_id_holder = Holders.SingleObjectHolder<Byte>()
        ret = agnos.olaContactlessFinalSelectCandidate(1, kernel_id_holder)
        if (ret != OlaError.OLA_OK) {
            Outcome.showOutcome(agnos, ret)
        }

        Log.v("Transaction", "doTransaction - doTransaction")
        ret = agnos.olaContactlessDoTransaction()
        Outcome.showOutcome(agnos, ret)
        if (ret == OlaError.OLA_OK) {
            val bytesHolder = Holders.SingleObjectHolder<ByteArray>()
            for (olaTag in outcomeTags) {
                val result = agnos.olaEmvGetTag(olaTag.tag, bytesHolder)
                if (result == OlaError.OLA_OK) {
                    Log.v("Transaction", "doTransaction - result for getTag(): $olaTag $result => " +
                          Utils.bytesToHex(bytesHolder.get()))
                    olaTagOutPut =olaTagOutPut + "-"+olaTag.nameIndicator+" : " +  Utils.bytesToHex(bytesHolder.get())+ "\n"




                } else {
                    Log.v("Transaction", "doTransaction - result for getTag(): $result => tag missing")

                }
            }
        }

        //if (!BuildConfig.FLAVOR.lowercase().contains("nosred")) {
            Sred.exportHashedPAN(agnos)
            Sred.exportKCV(agnos)
            Sred.decryptDataBlock(agnos)
        //}

        Log.d("Transaction", "doTransaction - **** REMOVE CARD **** ")

        txnGetUsefulInfo()

        agnos.olaContactlessClean();

        return ret
    }
}
