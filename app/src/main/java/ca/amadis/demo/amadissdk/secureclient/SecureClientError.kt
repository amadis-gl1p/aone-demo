

enum class SecureClientError(val value: Int, val message: String) {

    ERR_INV(0, "Unknown error"),
    ERR_INT(1, "Internal error"),
    ERR_STD(2, "STD library call failed"),
    ERR_NDK(3, "NDK error"),
    ERR_STR(4, "Data store error"),
    ERR_RES(5, "Insufficient resources"),
    ERR_JNI(6, "JNI error"),
    ERR_EXP(7, "JNI exception error"),
    ERR_CRY(8, "Regular crypto error"),
    ERR_WBC(9, "Whitebox crypto error"),
    ERR_TLS(10, "TLS error"),
    ERR_HTP(11, "HTTP error"),
    ERR_VAL(12, "Invalid value"),
    ERR_CNX(13, "TLS connection failed"),
    ERR_API(14, "Invalid API usage"),
    ERR_STE(15, "Invalid state");

    companion object {

        fun getErrorByValue(value: Int): SecureClientError {

            /*
             * 'value' can either be the actual value (from 0 to 15) or the error code sent by
             * the secure client (which contains random MSB digits)
             */
            val lsb = value and 0x0f
            return SecureClientError.values().first { it.value == lsb }
        }
    }
}