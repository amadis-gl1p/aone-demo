package ca.amadis.demo.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import ca.amadis.demo.data.TransactionDatabase
import ca.amadis.demo.data.TransactionEntity
import ca.amadis.demo.repository.TransactionRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class TransactionViewModel(application: Application): AndroidViewModel(application) {
    val readAllData: LiveData<List<TransactionEntity>>

    private val repository: TransactionRepository

    init {
        val transactionDao = TransactionDatabase.getDatabase(application).transactionDao()
        repository = TransactionRepository(transactionDao)
        readAllData = repository.readAllTransactions

    }


    fun addTransaction(transaction: TransactionEntity) {

        viewModelScope.launch(Dispatchers.IO) {
            repository.addTransaction(transaction)
        }

    }
}
