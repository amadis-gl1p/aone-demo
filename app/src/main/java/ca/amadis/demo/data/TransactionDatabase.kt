package ca.amadis.demo.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase


@Database(entities = [TransactionEntity::class],version =1,exportSchema = false )
abstract class TransactionDatabase : RoomDatabase() {

    abstract fun transactionDao():TransactionDao
    companion object{
        @Volatile
        private var INSTANCE:TransactionDatabase ? = null

        fun getDatabase(context: Context): TransactionDatabase {
            val tempIstance = INSTANCE
            if (tempIstance != null) {

                return tempIstance
            }

            kotlin.synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    TransactionDatabase::class.java,
                    "transaction_database"
                ).build()
                INSTANCE = instance
                return instance
            }

        }
    }

}