package ca.amadis.demo.data

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query


@Dao
interface TransactionDao {


    @Query("SELECT * FROM transaction_table ORDER BY id DESC")
    fun  readAllData (): LiveData<List<TransactionEntity>>

    @Query("SELECT * FROM transaction_table ORDER BY ID DESC LIMIT 1 ")
    fun findLastTransaction (): TransactionEntity

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun addTransaction(transaction : TransactionEntity)
}
