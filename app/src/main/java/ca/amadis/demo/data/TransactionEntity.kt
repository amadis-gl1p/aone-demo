package ca.amadis.demo.data

import androidx.recyclerview.widget.DiffUtil
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "transaction_table")
data class TransactionEntity(
    @PrimaryKey(autoGenerate = true)
    var id:Long,
    var status: String,
    var amount:String?,
    var time:String,
    var date:String,


)
object TransactionDiffCallback : DiffUtil.ItemCallback<TransactionEntity>() {
    override fun areItemsTheSame(oldItem: TransactionEntity, newItem: TransactionEntity) =
        oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: TransactionEntity, newItem: TransactionEntity) =
        oldItem == newItem
}
