/*
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ca.amadis.demo.ui.starttransaction

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import ca.amadis.demo.R
import ca.amadis.demo.databinding.FragmentComposeBinding
import ca.amadis.demo.ui.MainActivity


/**
 * A [Fragment] which allows for the starting of a new transaction.
 */
class StartTransactionFragment : Fragment() {

    private lateinit var binding: FragmentComposeBinding



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentComposeBinding.inflate(inflater, container, false)
        return binding.root
    }

    @SuppressLint("ResourceType")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        binding.run {
            fab.setOnClickListener{

                fab.isEnabled = false


                //loading our custom made animations
                var animation = AnimationUtils.loadAnimation(context, R.animator.fade_in)
                animation.duration = 5000
                it.startAnimation(animation)
                binding.closeIcon.isEnabled = false
                val amount = binding.bodyTextView.text.toString()

                MainActivity.getInstancem().startTransaction(amount)



            }
            closeIcon.setOnClickListener { findNavController().navigateUp() }

        }
    }



}