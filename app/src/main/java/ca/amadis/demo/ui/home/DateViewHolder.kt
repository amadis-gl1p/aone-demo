package ca.amadis.demo.ui.home
import androidx.recyclerview.widget.RecyclerView
import ca.amadis.demo.databinding.RegroupmentDateLayoutBinding
import ca.amadis.demo.data.TransactionEntity

class DateViewHolder(private val binding:RegroupmentDateLayoutBinding): RecyclerView.ViewHolder(binding.root) {


    fun bind(transaction: TransactionEntity) {
        binding.transaction = transaction
        binding.root.isActivated = true



        // Setting interpolation here controls whether or not we draw the top left corner as
        // rounded or squared. Since all other corners are set to 0dp rounded, they are
        // not affected.


        binding.executePendingBindings()
    }
}
