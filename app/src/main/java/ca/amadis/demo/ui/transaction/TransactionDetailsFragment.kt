package ca.amadis.demo.ui.transaction

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import ca.amadis.demo.R
import ca.amadis.demo.databinding.FragmentTransactionBinding
import ca.amadis.demo.data.TransactionEntity
import ca.amadis.demo.viewmodel.TransactionViewModel


/**
 * A simple [Fragment] subclass.
 * Use the [  TransactionFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class TransactionDetailsFragment : Fragment() {

    private val args: TransactionDetailsFragmentArgs by navArgs()
    private val transactionId: Long by lazy(LazyThreadSafetyMode.NONE) { args.transactionId }

    private lateinit var  transactionViewModel : TransactionViewModel

    lateinit var transaction : TransactionEntity

    private lateinit var binding: FragmentTransactionBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentTransactionBinding.inflate(inflater, container, false)
        return binding.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.navigationIcon.setOnClickListener {
            findNavController().navigate(R.id.homeFragment)
        }

        transactionViewModel = ViewModelProvider(this).get(TransactionViewModel::class.java)

        transactionViewModel.readAllData.observe(viewLifecycleOwner, Observer {

            binding.run {
                if(transactionId == -0L){
                    this.transaction = it.first()
                }else{this.transaction = it.find { it.id == transactionId}}


            }
        })


    }



}