package ca.amadis.demo.ui.home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import ca.amadis.demo.databinding.RegroupmentDateLayoutBinding
import ca.amadis.demo.databinding.TransactionItemLayoutBinding
import ca.amadis.demo.data.TransactionDiffCallback
import ca.amadis.demo.data.TransactionEntity


/**
 * Simple adapter to display Email's in MainActivity.
 */
class TransactionAdapter(
    private val listener: TransactionAdapterListener
) : ListAdapter<TransactionEntity, ViewHolder>(TransactionDiffCallback)
{

    interface TransactionAdapterListener {
        fun onTransactionClicked(cardView: View, transaction: TransactionEntity)
        fun onTransactionLongPressed(transaction: TransactionEntity): Boolean
        fun onTransactionStarChanged(transaction: TransactionEntity, newValue: Boolean)
        fun onTransactionArchived(transaction: TransactionEntity)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return if(viewType == 1){

        DateViewHolder(
            RegroupmentDateLayoutBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )}else TransactionViewHolder(
            TransactionItemLayoutBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            ),
            listener
        )
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if( holder is TransactionViewHolder){
        holder.bind(getItem(position))}
        else if ( holder is DateViewHolder){
         holder.bind(getItem(position))}

    }

    override fun getItemViewType(position: Int): Int {
        val element = getItem(position)
        return if (element.id.toInt() == 0) {
            1
        } else 2

    }
}