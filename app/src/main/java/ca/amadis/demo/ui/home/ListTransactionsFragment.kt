/*
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ca.amadis.demo.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import ca.amadis.demo.databinding.FragmentHomeBinding
import ca.amadis.demo.data.TransactionEntity
import ca.amadis.demo.ui.starttransaction.StartTransactionFragmentDirections
import ca.amadis.demo.viewmodel.TransactionViewModel

/**
 * A [Fragment] that displays a list of Transactions.
 */
class ListTransactionsFragment : Fragment(), TransactionAdapter.TransactionAdapterListener {



    private lateinit var binding: FragmentHomeBinding

    private val transactionAdapter = TransactionAdapter(this)

    private lateinit var  transactionViewModel: TransactionViewModel



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        binding.recyclerView.apply {

            adapter = transactionAdapter
        }
        binding.recyclerView.adapter = transactionAdapter

        transactionViewModel = ViewModelProvider(this).get(TransactionViewModel::class.java)
        transactionViewModel.readAllData.observe(viewLifecycleOwner, Observer { it->

            val listTransactions = mutableListOf<TransactionEntity>()
            val map = it.groupBy { it.date }.toMutableMap()

            map.keys.forEach { key ->
                listTransactions.add(TransactionEntity(0,"","","",key))
                for (i in map[key]!!)
                    listTransactions.add(i)

            }

            transactionAdapter.submitList(listTransactions)
        })

    }

    override fun onTransactionClicked(cardView: View, transaction: TransactionEntity) {
        // TODO: Set up MaterialElevationScale transition as exit and reenter transitions.
        val directions = ListTransactionsFragmentDirections.actionHomeFragmentToEmailFragment(transaction.id)
        findNavController().navigate(directions)
    }



    override fun onTransactionLongPressed(transaction: TransactionEntity): Boolean {

        return true
    }

    override fun onTransactionStarChanged(transaction: TransactionEntity, newValue: Boolean) {

    }

    override fun onTransactionArchived(transaction: TransactionEntity) {

    }
}
