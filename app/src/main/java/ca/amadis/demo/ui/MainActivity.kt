/*
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ca.amadis.demo.ui
import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import androidx.annotation.MenuRes
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import androidx.navigation.findNavController
import ca.amadis.agnos.sdk.Agnos
import ca.amadis.agnos.sdk.DevListener
import ca.amadis.agnos.sdk.Key
import ca.amadis.agnos.sdk.Rpc
import ca.amadis.agnos.sdk.ola.types.OlaOutcomeParameter
import ca.amadis.agnos.sdk.ola.utils.Holders
import ca.amadis.demo.R
import ca.amadis.demo.databinding.ActivityMainBinding
import ca.amadis.tester.agnos.Transaction
import ca.amadis.demo.amadissdk.secureclient.SecureClient
import ca.amadis.demo.amadissdk.secureclient.SecureClientListener
import ca.amadis.demo.data.TransactionEntity
import ca.amadis.demo.ui.nav.*
import ca.amadis.demo.util.contentView
import ca.amadis.demo.viewmodel.TransactionViewModel
import ca.amadis.demo.ui.starttransaction.StartTransactionFragmentDirections
import kotlinx.coroutines.*
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.nio.file.Files
import java.text.SimpleDateFormat
import java.util.*
import kotlin.LazyThreadSafetyMode.NONE


class MainActivity : AppCompatActivity(),
                     Toolbar.OnMenuItemClickListener,
                     NavController.OnDestinationChangedListener,
    SecureClientListener, FragmentToActivity, DevListener

{
    companion object {

        lateinit var instance : MainActivity

        fun getInstancem() : MainActivity {

            return instance
        }
    }

    private lateinit var  transactionViewModel : TransactionViewModel
    var job: Job? = null

    private val binding: ActivityMainBinding by contentView(R.layout.activity_main)
    private val bottomNavDrawer: BottomNavDrawerFragment by lazy(NONE) {
        supportFragmentManager.findFragmentById(R.id.bottom_nav_drawer) as BottomNavDrawerFragment
    }


    private var secureClient: SecureClient? = null
    private var agnos: Agnos? = null
    private var purchase: Transaction? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_FULLSCREEN
                or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY)
        transactionViewModel = ViewModelProvider(this).get(TransactionViewModel::class.java)
        setUpBottomNavigationAndFab()
        /* Security assets management */
        val src = "amadis/secclt"
        val dest = "$filesDir/assets/$src"
        deleteAssets(dest)
        copyAssets(src,  dest)

        if (secureClient == null) {

            secureClient = SecureClient(this, this, this)
        }

        instance = this

    }
    private fun deleteAssets(path: String) {

        Log.i("MainActivity", "--- deleteAssets ---")

        val f = File(path)
        if (Files.exists(f.toPath())) {
            /* Remove */
            Log.d("MainActivity", "remove")
            f.deleteRecursively()
        }
    }

    private fun copyAssets(src: String, dest: String) {

        Log.i("MainActivity", "--- copyAssets ---")

        val bufferSize = 8192

        /* Create destination folder */
        File(dest).mkdirs()

        /* Get list of assets */
        val assets: Array<String> = this.assets.list(src) as Array<String>
        Log.i("MainActivity", "${assets.size}")

        /* Copy each asset */
        for (asset in assets) {
            val file = "$dest/$asset"
            try {
                val stream = this.assets.open("$src/$asset")
                FileOutputStream(File(file), false).use { outputStream ->
                    var read: Int
                    val bytes = ByteArray(bufferSize)
                    while (stream.read(bytes).also { read = it } != -1)
                        outputStream.write(bytes, 0, read)
                }

                stream.close()
            } catch (ex: FileNotFoundException) {
                ex.printStackTrace()
                continue
            }
        }
    }
    override fun onStart() {
        super.onStart()
        CoroutineScope(Dispatchers.IO).launch {
            if (secureClient?.isAuthenticated() == ca.amadis.secclt.sdk.SecureClient.AuthState.AUTH_STATE_UNAUTH.value) {


                    println("Secure client authentication...")
                    secureClient?.authenticate()


            } else if (!secureClient?.isProvisioned()!!) {

                 onAuthenticateDone()

            } else if (!secureClient?.isInitialized()!!) {

                onProvisionDone()

            } else {

                 initializeAgnos()
            }
        }
    }
    private fun initializeAgnos() {

        Log.v("MainActivity", "--- initializeAgnos ---")

        println("Agnos initialization...")

        if (agnos == null) {

            agnos = Agnos(this)
            agnos!!.setActivity(this)
            agnos?.initialize()
        }
    }


    private fun setUpBottomNavigationAndFab() {
        // Wrap binding.run to ensure ContentViewBindingDelegate is calling this Activity's
        // setContentView before accessing views
        binding.run {
            findNavController(R.id.nav_host_fragment).addOnDestinationChangedListener(
                this@MainActivity
            )
        }

        // Set a custom animation for showing and hiding the FAB
        binding.fab.apply {
            setShowMotionSpecResource(R.animator.fab_show)
            setHideMotionSpecResource(R.animator.fab_hide)
            setOnClickListener {
                navigateToCompose()
            }
        }

        bottomNavDrawer.apply {
            addOnSlideAction(HalfClockwiseRotateSlideAction(binding.bottomAppBarChevron))
            addOnSlideAction(AlphaSlideAction(binding.bottomAppBarTitle, true))
            addOnStateChangedAction(ShowHideFabStateAction(binding.fab))
            addOnStateChangedAction(ChangeSettingsMenuStateAction { showSettings ->
                // Toggle between the current destination's BAB menu and the menu which should
                // be displayed when the BottomNavigationDrawer is open.
                binding.bottomAppBar.replaceMenu(if (showSettings) {
                    R.menu.bottom_app_bar_settings_menu
                } else {
                    getBottomAppBarMenuForDestination()
                })
            })

            addOnSandwichSlideAction(HalfCounterClockwiseRotateSlideAction(binding.bottomAppBarChevron))
//            addNavigationListener(this@MainActivity)
        }

        // Set up the BottomAppBar menu
        binding.bottomAppBar.apply {
            setNavigationOnClickListener {
                bottomNavDrawer.toggle()
            }
            setOnMenuItemClickListener(this@MainActivity)
        }

        // Set up the BottomNavigationDrawer's open/close affordance
        binding.bottomAppBarContentContainer.setOnClickListener {
            bottomNavDrawer.toggle()
        }
    }

    override fun onDestinationChanged(
        controller: NavController,
        destination: NavDestination,
        arguments: Bundle?
    ) {
        // Set the currentEmail being viewed so when the FAB is pressed, the correct email
        // reply is created. In a real app, this should be done in a ViewModel but is done
        // here to keep things simple. Here we're also setting the configuration of the
        // BottomAppBar and FAB based on the current destination.
        when (destination.id) {
            R.id.homeFragment -> {

                setBottomAppBarForHome(getBottomAppBarMenuForDestination(destination))
            }

            R.id.composeFragment -> {

                setBottomAppBarForCompose()
            }

            R.id.transactionFragment -> {
                binding.fab.visibility = View.INVISIBLE
                bottomNavDrawer.close()
                setBottomAppBarForCompose()
            }

        }
    }

    /**
     * Helper function which returns the menu which should be displayed for the current
     * destination.
     *
     * Used both when the destination has changed, centralizing destination-to-menu mapping, as
     * well as switching between the alternate menu used when the BottomNavigationDrawer is
     * open and closed.
     */
    @MenuRes
    private fun getBottomAppBarMenuForDestination(destination: NavDestination? = null): Int {
        val dest = destination ?: findNavController(R.id.nav_host_fragment).currentDestination
        return when (dest?.id) {
            R.id.homeFragment -> R.menu.bottom_app_bar_home_menu
            else -> R.menu.bottom_app_bar_home_menu
        }
    }

    private fun setBottomAppBarForHome(@MenuRes menuRes: Int) {
        binding.run {
            fab.setImageState(intArrayOf(-android.R.attr.state_activated), true)
            bottomAppBar.visibility = View.VISIBLE
            bottomAppBar.replaceMenu(menuRes)

            //bottomAppBarTitle.visibility = View.VISIBLE
            bottomAppBar.performShow()
            fab.show()
        }
    }



    private fun setBottomAppBarForCompose() {
        hideBottomAppBar()

        binding.fab.visibility = View.INVISIBLE

    }



    private fun hideBottomAppBar() {
        binding.run {

            bottomAppBar.performHide()
            // Get a handle on the animator that hides the bottom app bar so we can wait to hide
            // the fab and bottom app bar until after it's exit animation finishes.
            bottomAppBar.animate().setListener(object : AnimatorListenerAdapter() {
                var isCanceled = false
                override fun onAnimationEnd(animation: Animator) {
                    if (isCanceled) return

                    // Hide the BottomAppBar to avoid it showing above the keyboard
                    // when composing a new email.
                    bottomAppBar.visibility = View.GONE
                    fab.visibility = View.INVISIBLE
                }
                override fun onAnimationCancel(animation: Animator) {
                    isCanceled = true
                }
            })
        }
    }


    override fun onMenuItemClick(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.menu_settings -> {
                bottomNavDrawer.close()
                showAgnosVersion()
            }

        }
        return true
    }

    private fun showAgnosVersion() {
        initializeAgnos()
        val versions: String = agnos!!.olaAgnosVersions

        MenuBottomSheetDialogFragment
          .newInstance(R.menu.dark_theme_bottom_sheet_menu, versions)
          .show(supportFragmentManager, null)
    }


    private fun navigateToCompose() {
        // TODO: Set up MaterialElevationScale transition as exit and reenter transitions.
        val directions = StartTransactionFragmentDirections.actionGlobalComposeFragment(1L)
        findNavController(R.id.nav_host_fragment).navigate(directions)
    }



    override fun onAuthenticateDone() {

        /* Secure client authentication has been performed, provision the client */
        Log.v("MainActivity", "--- onAuthenticateDone ---")

        println(secureClient?.isProvisioned())

        if (!secureClient?.isProvisioned()!!) {
            println("Secure client provisioning...")
            secureClient?.provision(true)
        }

        println(secureClient?.isInitialized())
        if (!secureClient?.isInitialized()!!) {
            println("Secure client initialization...")
            secureClient?.initialize()
        }
    }

    override fun onAuthenticateFailed(error: Int, message: String) {

        /* Secure client authentication failed, stop here */
        Log.v("MainActivity", "--- onAuthenticateFailed ---");
        Log.v("MainActivity", "error: $error, message: $message")
        finishAffinity()

        println("Secure client authentication failed!")
    }

    override fun onProvisionDone() {

        /* Secure client provisioning has been performed, initialize the client */
        Log.v("MainActivity", "--- onProvisionDone ---")

        print("Secure client initialization...")

        secureClient?.initialize()
    }

    override fun onProvisionFailed(error: Int, message: String) {

        /* Secure client provisioning failed, stop here */
        Log.v("MainActivity", "--- onProvisionFailed ---")
        Log.v("MainActivity", "error: $error, message: $message")

        println("Secure client provisioning failed!")
    }

    override fun onInitializeDone() {

        /* Secure client initialization has been performed, start attestation and monitoring */
        Log.v("MainActivity", "--- onInitializeDone ---")

        println("Secure client attestation & monitoring...")

        secureClient?.startAttestationAndMonitoring()
    }

    override fun onInitializeFailed(error: Int, message: String) {


    }

    override fun onStartDone() {

        /* Secure client attestaion and monitoring service started, initialize the rest... */
        Log.v("MainActivity", "--- onStartDone ---")
        initializeAgnos()
        purchase = Transaction(this, agnos!!)
        runOnUiThread {
            binding.fab.isEnabled = true
            binding.myProgressBar.visibility = View.INVISIBLE
        }

    }

    override fun onTerminateDone() {

        Log.v("MainActivity", "--- onTerminateDone ---")
    }



    override fun onPINDone() {

        Log.v("MainActivity", "--- onPINDone ---")

        waitAndClear()
    }

    /* Other fun */

    fun waitAndClear() {

        /* Clear everything after 5 sec */

    }

    override fun startTransaction(amount: String?) {
        if (amount != null) {
            purchase?.setAmount(amount.replace("""[$,.]""".toRegex(), "").toLong())
        }


        if (job != null) job = null
        purchase = null

        var result : Holders.SingleObjectHolder<OlaOutcomeParameter>
        val sdf = SimpleDateFormat("dd/M/yyyy hh:mm:ss")
        val currentDate = sdf.format(Date())


        initializeAgnos()

        purchase = Transaction(this, agnos!!)
        job = CoroutineScope(Dispatchers.IO).launch {

            val output  = purchase!!.run().get()
            transactionViewModel.addTransaction(
                TransactionEntity(0,
                """
                |${output?.outcome?.name} 
                |-Card Verification Method: ${ output?.cvm}
                |${purchase!!.olaTagOutPut}
                |""".trimMargin(),
                "Transaction $amount",
                currentDate.subSequence(10..17).toString(),
                currentDate.subSequence(0..8).toString(),
                )
            )


            withContext(Dispatchers.Main){

                val directions = StartTransactionFragmentDirections.actionComposeFragmentToTransactionFragment(-0L)
                findNavController(R.id.nav_host_fragment).navigate(directions)

            }
        }


        agnos = null


    }


    override fun handleDevEvent(data: ByteArray) {

        val cmd = data[0].toInt()

        /* Handles NFC requests locally. Pass the rest onto the higher level app */
        if (cmd == Rpc.Cmd.RPC_CMDID_EVT_LOG.id) {

            if (data.size >= 2) {

                val msg = data.copyOfRange(2, data.size - 1)
                Log.v("ola-app", "dev log: " + String(msg))
            }
        }
    }

    override fun handleDevRequest(data: ByteArray): ByteArray {

        Log.v("MainActivity", "--- handleDevRequest ---")

        val cmd = data[0].toInt()

//        /* Handle CANCEL event only */
        return if (cmd == Rpc.Cmd.RPC_CMDID_REQ_PINPAD_GETKEY.id) {

             byteArrayOf(Key.PINPAD_NO_KEY.value.toByte())

        } else {

            ByteArray(0)
        }

    }

    override fun cardPresent() {

        Log.v("MainActivity", "--- cardPresent ---")
    }

    override fun cardRemoved() {


        Log.v("MainActivity", "--- cardRemoved ---")
        job?.cancel()
    }
    override fun initializeCompleted() {


        Log.v("MainActivity", " --- initializeCompleted  ---")





    }
    override fun initializeFailed(message : String) {


        Log.v("MainActivity", "--- initializeFailed --- + $message" )
    }
}
