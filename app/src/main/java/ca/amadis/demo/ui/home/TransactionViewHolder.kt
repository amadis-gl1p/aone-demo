package ca.amadis.demo.ui.home
import androidx.recyclerview.widget.RecyclerView
import ca.amadis.demo.R
import ca.amadis.demo.databinding.TransactionItemLayoutBinding
import ca.amadis.demo.data.TransactionEntity


class TransactionViewHolder(
    private val binding: TransactionItemLayoutBinding,
    listener: TransactionAdapter.TransactionAdapterListener
): RecyclerView.ViewHolder(binding.root) {



    private val starredCornerSize =
        itemView.resources.getDimension(R.dimen.reply_small_component_corner_radius)



    init {
        binding.run {
            this.listener = listener
        }
    }

    fun bind(transaction: TransactionEntity) {
        binding.transaction = transaction
        binding.root.isActivated = true



        // Setting interpolation here controls whether or not we draw the top left corner as
        // rounded or squared. Since all other corners are set to 0dp rounded, they are
        // not affected.
        val interpolation = 1F
        updateCardViewTopLeftCornerSize(interpolation)

        binding.executePendingBindings()
    }



    // We have to update the shape appearance itself to have the MaterialContainerTransform pick up
    // the correct shape appearance, since it doesn't have access to the MaterialShapeDrawable
    // interpolation. If you don't need this work around, prefer using MaterialShapeDrawable's
    // interpolation property, or in the case of MaterialCardView, the progress property.
    private fun updateCardViewTopLeftCornerSize(interpolation: Float) {
        binding.cardView.apply {
            shapeAppearanceModel = shapeAppearanceModel.toBuilder()
                .setTopLeftCornerSize(interpolation * starredCornerSize)
                .build()
        }
    }
}