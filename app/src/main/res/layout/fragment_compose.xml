<?xml version="1.0" encoding="utf-8"?>
<!--
  Copyright (c) 2020 Google Inc.

  Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
  in compliance with the License. You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software distributed under the License
  is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
  or implied. See the License for the specific language governing permissions and limitations under
  the License.
  -->
<layout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools">



    <androidx.core.widget.NestedScrollView
        android:id="@+id/nested_scroll_view"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:fillViewport="true"
        android:paddingHorizontal="@dimen/grid_0_5"
        android:paddingTop="@dimen/grid_1"
        android:clipToPadding="false"
        android:background="?android:colorBackground"
        app:paddingTopSystemWindowInsets="@{true}">

        <com.google.android.material.card.MaterialCardView
            android:id="@+id/email_card_view"
            android:layout_width="match_parent"
            android:layout_height="wrap_content">

            <androidx.constraintlayout.widget.ConstraintLayout
                android:id="@+id/compose_constraint_layout"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:layout_marginTop="@dimen/grid_2">

                <ImageButton
                    android:id="@+id/close_icon"
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toTopOf="parent"
                    android:padding="@dimen/min_icon_target_padding"
                    android:layout_marginStart="@dimen/grid_1"
                    android:contentDescription="@string/compose_close_content_desc"
                    android:background="?attr/actionBarItemBackground"
                    app:srcCompat="@drawable/ic_close"
                    app:tint="@color/color_on_surface_emphasis_disabled" />

                <TextView
                    android:id="@+id/subject_edit_text"
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:minHeight="@dimen/min_touch_target_size"
                    android:layout_marginHorizontal="@dimen/grid_2"
                    android:paddingVertical="@dimen/grid_0_25"
                    app:layout_constraintEnd_toStartOf="@+id/send_icon"
                    app:layout_constraintStart_toEndOf="@id/close_icon"
                    app:layout_constraintTop_toTopOf="parent"
                    android:background="@android:color/transparent"
                    android:hint="@string/compose_subject_hint"
                    android:text="@string/transaction"

                    android:textAppearance="?attr/textAppearanceHeadline5" />

                <ImageButton
                    android:id="@+id/send_icon"
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    android:padding="@dimen/min_icon_target_padding"
                    android:layout_marginEnd="@dimen/grid_1"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintTop_toTopOf="parent"
                    android:contentDescription="@string/compose_send_content_desc"
                    android:background="?attr/actionBarItemBackground"
                    android:clickable="true"
                    android:focusable="true"
                    app:tint="?attr/colorPrimary"
                    app:srcCompat="@drawable/ic_twotone_send"
                    android:visibility="invisible"
                    />

                <ImageView
                    android:id="@+id/subject_divider"
                    android:layout_width="match_parent"
                    android:layout_height="1dp"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@+id/subject_edit_text"
                    app:layout_constraintEnd_toEndOf="parent"
                    android:layout_marginTop="@dimen/grid_1"
                    android:layout_marginHorizontal="@dimen/grid_2"
                    android:background="@drawable/divider"
                    app:backgroundTint="@color/color_on_surface_divider"
                    android:visibility="invisible"
                    />

                <Spinner
                    android:id="@+id/sender_spinner"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintTop_toBottomOf="@id/subject_divider"
                    android:layout_marginTop="@dimen/grid_0_5"
                    android:layout_marginEnd="@dimen/grid_1"
                    android:foreground="?attr/selectableItemBackground"
                    app:popupElevationOverlay="@{@dimen/plane_16}"
                    android:visibility="invisible"

                    />

                <ImageView
                    android:id="@+id/sender_divider"
                    android:layout_width="match_parent"
                    android:layout_height="1dp"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@+id/sender_spinner"
                    android:layout_marginHorizontal="@dimen/grid_2"
                    android:layout_marginTop="@dimen/grid_0_5"
                    android:background="@drawable/divider"
                    app:backgroundTint="@color/color_on_surface_divider"
                    android:visibility="invisible"
                    />

                <HorizontalScrollView
                    android:id="@+id/recipient_scroll_view"
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    app:layout_constraintEnd_toStartOf="@id/recipient_add_icon"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@id/sender_divider"
                    android:layout_marginTop="@dimen/grid_1"
                    android:layout_marginEnd="@dimen/grid_2"
                    android:minHeight="48dp"
                    android:paddingVertical="@dimen/grid_0_25"
                    android:clipToPadding="false"
                    android:scrollbars="none"
                    android:requiresFadingEdge="horizontal"
                    android:fadingEdgeLength="@dimen/grid_3"
                    android:overScrollMode="never"
                    android:contentDescription="@string/compose_recipient_group_content_desc"
                    android:visibility="invisible">

                    <com.google.android.material.chip.ChipGroup
                        android:id="@+id/recipient_chip_group"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:paddingStart="@dimen/grid_2"
                        android:scrollbars="none"
                        app:singleLine="true" />

                </HorizontalScrollView>

                <ImageButton
                    android:id="@+id/recipient_add_icon"
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintTop_toTopOf="@id/recipient_scroll_view"
                    app:layout_constraintBottom_toBottomOf="@id/recipient_scroll_view"
                    android:layout_marginEnd="@dimen/grid_1"
                    android:padding="@dimen/min_icon_target_padding"
                    android:contentDescription="@string/compose_add_recipient_icon_content_desc"
                    android:background="?attr/actionBarItemBackground"
                    android:clickable="true"
                    android:focusable="true"
                    app:tint="@color/color_on_surface_emphasis_disabled"
                    app:srcCompat="@drawable/ic_twotone_add_circle_outline"
                    android:visibility="invisible"

                    />

                <ImageView
                    android:id="@+id/recipient_divider"
                    android:layout_width="match_parent"
                    android:layout_height="1dp"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@+id/recipient_scroll_view"
                    app:layout_constraintEnd_toEndOf="parent"
                    android:layout_marginTop="@dimen/grid_1"
                    android:layout_marginHorizontal="@dimen/grid_2"
                    android:background="@drawable/divider"
                    app:backgroundTint="@color/color_on_surface_divider"
                    android:visibility="invisible"
                    />
                <TextView
                    android:id="@+id/amount_to_pay"
                    android:layout_width="match_parent"
                    android:layout_height="30dp"
                    android:gravity="center"

                    android:layout_marginHorizontal="@dimen/grid_2"

                    app:layout_constraintTop_toBottomOf="@id/subject_edit_text"
                    app:layout_constraintEnd_toEndOf="parent"
                    android:text="@string/set_the_amount_to_pay"
                    android:textAppearance="?attr/textAppearanceBody1"
                    android:textColor="@color/color_on_surface_emphasis_high"
                    android:drawablePadding="@dimen/grid_3"
                    android:lines="3"
                    android:ellipsize="end"
                    tools:text="hikingfan@gmail.com" />

                <EditText
                    android:id="@+id/body_text_view"

                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@id/amount_to_pay"
                    app:layout_constraintEnd_toEndOf="parent"
                    android:layout_marginTop="@dimen/grid_4"
                    android:layout_marginHorizontal="@dimen/grid_2"

                    android:background="@android:color/transparent"


                    android:gravity="top"
                    android:textAppearance="?attr/textAppearanceBody1"
                    app:lineHeight="24sp"
                    android:layout_height="80dp"
                    android:layout_width="match_parent"
                    android:layout_gravity="center"
                    android:textAlignment="center"
                    android:layout_row="1"
                    android:layout_column="0"
                    android:layout_columnSpan="4"
                    android:textColor="#0A4066"
                    android:textSize="20sp"
                    android:text="@string/_1_00"
                    android:inputType="numberDecimal"
                    android:numeric="decimal"

                    />
                <TextView
                    android:id="@+id/ps"
                    android:layout_width="match_parent"
                    android:layout_height="match_parent"
                    android:gravity="center"

                    android:layout_marginHorizontal="@dimen/grid_2"
                    android:paddingBottom="@dimen/grid_4"
                    app:layout_constraintTop_toBottomOf="@id/body_text_view"
                    app:layout_constraintEnd_toEndOf="parent"
                    android:text="@string/click_the_button_to_start_the_transaction"

                    android:textAppearance="?attr/textAppearanceBody1"
                    android:textColor="@color/color_on_surface_emphasis_high"
                    android:drawablePadding="@dimen/grid_3"
                    android:lines="3"
                    android:ellipsize="end"
                    tools:text="hikingfan@gmail.com" />

                <LinearLayout
                    android:id="@+id/fabb"
                    android:layout_width="wrap_content"
                    android:layout_height="150dp"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@id/ps"

                    app:layout_constraintEnd_toEndOf="parent"
                    android:layout_marginTop="@dimen/grid_2"
                    android:layout_marginHorizontal="@dimen/grid_2"
                    android:paddingBottom="@dimen/grid_4"
                    android:orientation="horizontal"
                    android:padding="8dp"
                    android:clipToPadding="false"
                    >


                <com.google.android.material.floatingactionbutton.FloatingActionButton
                    android:id="@+id/fab"
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"


                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@id/recipient_divider"
                    app:layout_constraintEnd_toEndOf="parent"
                    android:layout_marginTop="@dimen/grid_2"
                    android:layout_marginHorizontal="@dimen/grid_2"
                    android:paddingBottom="@dimen/grid_4"

                    android:contentDescription="@string/fab_compose_email_content_description"
                    app:srcCompat="@drawable/baseline_nfc_24"

                    />


                </LinearLayout>
                <TextView
                    android:id="@+id/account_address_t"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:layout_marginHorizontal="@dimen/grid_2"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintTop_toBottomOf="@id/fabb"


                    android:gravity="center"


                    android:text="@string/ps_after_starting_the_transaction_please_gently_slide_the_card_on_the_back_of_the_phone_until_it_vibrates"

                    android:textAppearance="?attr/textAppearanceBody1"
                    android:textColor="@color/color_on_surface_emphasis_high"
                    android:drawablePadding="@dimen/grid_3"
                    android:lines="3"
                    android:ellipsize="end"
                    tools:text="hikingfan@gmail.com" />


                <LinearLayout
                    android:id="@+id/fab2"
                    android:layout_width="wrap_content"
                    android:layout_height="80dp"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@id/account_address_t"
                    app:layout_constraintEnd_toEndOf="parent"

                    android:layout_marginTop="@dimen/grid_2"
                    android:layout_marginHorizontal="@dimen/grid_2"

                    android:orientation="horizontal"
                    android:padding="8dp"
                    android:clipToPadding="false"
                    >
                <ImageView
                    android:id="@+id/fabvisa"
                    android:layout_width="55dp"
                    android:layout_height="match_parent"
                    android:minHeight="400dp"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@id/fabb"
                    app:layout_constraintEnd_toEndOf="parent"
                    android:layout_marginTop="@dimen/grid_2"
                    android:layout_marginHorizontal="@dimen/grid_2"



                    app:srcCompat="@drawable/visa_svgrepo_com" />
                    <ImageView
                        android:id="@+id/fabmastercard"
                        android:layout_width="55dp"
                        android:layout_height="match_parent"
                        android:minHeight="400dp"
                        app:layout_constraintStart_toStartOf="parent"
                        app:layout_constraintTop_toBottomOf="@id/fabb"
                        app:layout_constraintEnd_toEndOf="parent"
                        android:layout_marginTop="@dimen/grid_2"
                        android:layout_marginHorizontal="@dimen/grid_2"
                        app:srcCompat="@drawable/mastercard_full_svgrepo_com" />
                    <ImageView
                        android:id="@+id/fabamex"
                        android:layout_width="55dp"
                        android:layout_height="match_parent"
                        android:minHeight="400dp"
                        app:layout_constraintStart_toStartOf="parent"
                        app:layout_constraintTop_toBottomOf="@id/fabb"
                        app:layout_constraintEnd_toEndOf="parent"
                        android:layout_marginTop="@dimen/grid_2"
                        android:layout_marginHorizontal="@dimen/grid_2"
                        app:srcCompat="@drawable/amex_svgrepo_com" />
                    <ImageView
                        android:id="@+id/fabajmc"
                        android:layout_width="55dp"
                        android:layout_height="match_parent"
                        android:minHeight="400dp"
                        app:layout_constraintStart_toStartOf="parent"
                        app:layout_constraintTop_toBottomOf="@id/fabb"
                        app:layout_constraintEnd_toEndOf="parent"
                        android:layout_marginTop="@dimen/grid_2"
                        android:layout_marginHorizontal="@dimen/grid_2"
                        app:srcCompat="@drawable/jcb_svgrepo_com" />
                    <ImageView
                        android:id="@+id/fabadiscover"
                        android:layout_width="55dp"
                        android:layout_height="match_parent"
                        android:minHeight="400dp"
                        app:layout_constraintStart_toStartOf="parent"
                        app:layout_constraintTop_toBottomOf="@id/fabb"
                        app:layout_constraintEnd_toEndOf="parent"
                        android:layout_marginTop="@dimen/grid_2"
                        android:layout_marginHorizontal="@dimen/grid_2"
                        app:srcCompat="@drawable/discover_svgrepo_com" />
                </LinearLayout>
                <ImageView
                    android:layout_width="wrap_content"
                    android:layout_height="60dp"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintBottom_toBottomOf="parent"
                    android:layout_marginHorizontal="@dimen/grid_2"
                    android:layout_marginEnd="@dimen/grid_2"
                    android:layout_marginTop="60dp"
                    app:layout_constraintTop_toBottomOf="@id/fab2"
                    app:srcCompat="@drawable/a_one_logo_horizontal_4c_baseline"

                     />



            </androidx.constraintlayout.widget.ConstraintLayout>

        </com.google.android.material.card.MaterialCardView>

    </androidx.core.widget.NestedScrollView>

</layout>
