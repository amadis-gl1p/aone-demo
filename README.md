# Demo application based on Secure Client and Agnos SDK's for Tap to/on Phone card processing

# Before use
Before you can use the demo application, a few pieces of information need to be set in the project:
* In app/src/main:
  * create directories assets/secclt
  * add the keys and certificates required for certificates pinning (sc_ca.pem, sc_client.crt, sc_client.key and sc_server.crt - see the corresponding wiki page to understand the content of these files)
* In gradle.properties:
  * set 'artifactory_url' to the JFrog repository's URL (ask for our wiki access)
  * set 'artifactory_user' and 'artifactory_password' with your credentials (ask for them to an Preludd staff member) 
* In SecureClient.kt:
  * set the 'settings' provisioning string (ask for our wiki access to understand the content of this parameter)
* In Sred.kt:
  * set the 'privateKey' variable to a correct value (either the Amadis demo key if using the Amadis demo Secure Backend or your own key)